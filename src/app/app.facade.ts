
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import * as type from '@entities/all-entities';
import * as actions from '@store/actions/actions-app';

import { State } from '@store/state/state-app';

@Injectable({
  providedIn: 'root'
})
export class AppFacade {

  constructor(private store: Store<State>) { }

  get users$(): Observable<type.User[]> {
    return this.store.select('users');
  }

  get userSelected$(): Observable<type.User> {
    return this.store.select('userSelected');
  }

  get favorites$(): Observable<type.User[]> {
    return this.store.select('favorites');
  }

  public fetchUsers(): void {
    this.store.dispatch(actions.fetchUsers());
  }

  public gethUserById(id: number): void {
    this.store.dispatch(actions.getUserById({ id }));
  }

  public createUser(user: type.User): void {
    this.store.dispatch(actions.createUser({ user }));
  }

  public updateUser(user: type.User): void {
    this.store.dispatch(actions.updateUser({ user }));
  }

  public deleteUser(id: number): void {
    this.store.dispatch(actions.deleteUser({ id }));
  }

  public sendFavorites(users: type.User[]): void {
    this.store.dispatch(actions.sendFavorites({ users }));
  }

  public changeFavorite(user: type.User): void {
    this.store.dispatch(actions.changeFavorite({ user }));
  }

}
