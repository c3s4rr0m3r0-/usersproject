import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { catchError, map, switchMap } from 'rxjs/operators';

import * as actions from '@store/actions/actions-app';
import { CoreService } from '@services/core.service';
import { Router } from '@angular/router';

@Injectable()
export class UsersEffects {

  constructor(
    private actions$: Actions,
    private core: CoreService,
    private router: Router) { }

  fetchUsers$ = createEffect(() => this.actions$.pipe(
    ofType(actions.fetchUsers),
    switchMap(() => this.core.getUsers().pipe(
      map((users) => actions.fetchUsersSuccess({ users }))
    ))
  ));

  getUsersById$ = createEffect(() => this.actions$.pipe(
    ofType(actions.getUserById),
    switchMap(({ id }) => this.core.getUserById(id).pipe(
      // tslint:disable-next-line: no-shadowed-variable
      map((user) => actions.getUserByIdSuccess({ user }))
    ))
  ));

  createUser$ = createEffect(() => this.actions$.pipe(
    ofType(actions.createUser),
    switchMap(({ user }) => this.core.createUser(user).pipe(
      map(() => {
        this.router.navigate(['/dashboard']);
        return actions.createUserSuccess();
      })
    ))
  ));

  updateUser$ = createEffect(() => this.actions$.pipe(
    ofType(actions.updateUser),
    switchMap(({ user }) => this.core.updateUser(user).pipe(
      map(() => {
        this.router.navigate(['/dashboard']);
        return actions.updateUserSuccess();
      })
    ))
  ));

  deleteUser$ = createEffect(() => this.actions$.pipe(
    ofType(actions.deleteUser),
    switchMap(({ id }) => this.core.deleteUser(id).pipe(
      switchMap(() => {
        const defaultActions: Action[] = [
          actions.fetchUsers(),
          actions.deleteUserSuccess()
        ];
        return defaultActions;
      })
    ))
  ));

  changeFavorite$ = createEffect(() => this.actions$.pipe(
    ofType(actions.changeFavorite),
    switchMap(({ user }) => this.core.updateUser(user).pipe(
      switchMap(() => {
        const defaultActions: Action[] = [
          actions.fetchUsers(),
          actions.changeFavoriteSuccess()
        ];
        return defaultActions;
      })
    ))
  ));

}
