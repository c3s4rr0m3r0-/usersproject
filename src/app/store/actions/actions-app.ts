import { createAction, props } from '@ngrx/store';

import { User } from '@entities/all-entities';

// Fetch users
export const fetchUsers = createAction('[Global/API] Fetch Users');
export const clearFetchUsers = createAction('[Global/API] Clear Fetch Users');
export const fetchUsersSuccess = createAction(
    '[Global/API] Fetch Users Success',
    props<{ users: User[] }>()
);

// Get user by id
export const getUserById = createAction(
    '[Global/API] Get User By Id',
    props<{ id: number }>()
);
export const getUserByIdSuccess = createAction(
    '[Global/API] Get User By Id Success',
    props<{ user: User }>()
);

// Create user
export const createUser = createAction(
    '[Global/API] Creater User',
    props<{ user: User }>()
);
export const createUserSuccess = createAction('[Global/API] Creater User Success');

// Update user
export const updateUser = createAction(
    '[Global/API] Update User',
    props<{ user: User }>()
);
export const updateUserSuccess = createAction('[Global/API] Update User Success');

// Delete user
export const deleteUser = createAction(
    '[Global/API] Delete User',
    props<{ id: number }>()
);
export const deleteUserSuccess = createAction('[Global/API] Delete User Success');

// Favorites

export const sendFavorites = createAction(
    '[Global/API] Send Favorites',
    props<{ users: User[] }>()
);

export const changeFavorite = createAction(
    '[Global/API] Change Favorite',
    props<{ user: User }>()
);
export const changeFavoriteSuccess = createAction('[Global/API] Change Favorite Success');

