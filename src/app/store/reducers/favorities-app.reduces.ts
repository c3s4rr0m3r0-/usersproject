import { createReducer, on } from '@ngrx/store';
import * as actions from '@store/actions/actions-app';

import * as type from '@entities/all-entities';

export const initialState = [] as type.User[];

// tslint:disable-next-line: variable-name
const _favoritesReducer = createReducer(
  initialState,
  on(actions.sendFavorites, (state, { users }) => users),
);

// tslint:disable-next-line: typedef
export function favoritesReducer(state, action) {
  return _favoritesReducer(state, action);
}
