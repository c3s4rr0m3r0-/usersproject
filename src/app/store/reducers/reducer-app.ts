import { createReducer, on } from '@ngrx/store';
import * as actions from '@store/actions/actions-app';

import * as type from '@entities/all-entities';

export const initialState = [] as type.User[];

// tslint:disable-next-line: variable-name
const _usersReducer = createReducer(
  initialState,
  on(actions.fetchUsersSuccess, (state, { users }) => users),
  on(actions.clearFetchUsers, (state) => {
    return [] as type.User[];
  })
);

// tslint:disable-next-line: typedef
export function usersReducer(state, action) {
  return _usersReducer(state, action);
}
