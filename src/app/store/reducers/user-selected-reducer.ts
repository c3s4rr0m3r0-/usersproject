
import { createReducer, on } from '@ngrx/store';
import * as actions from '@store/actions/actions-app';

import * as type from '@entities/all-entities';

export const initialState = {} as type.User;

// tslint:disable-next-line: variable-name
const _userSelectdReducer = createReducer(
  initialState,
  on(actions.getUserByIdSuccess, (state, { user }) => user),
);

// tslint:disable-next-line: typedef
export function userSelectdReducer(state, action) {
  return _userSelectdReducer(state, action);
}
