import { usersReducer } from '@store/reducers/reducer-app';
import { userSelectdReducer } from '../reducers/user-selected-reducer';
import { favoritesReducer } from '../reducers/favorities-app.reduces';

import { User } from '@entities/all-entities';

export type State = Readonly<{
    users: User[];
    userSelected: User;
    favorites: User[];
}>;

export const StateApp = {
    users: usersReducer,
    userSelected: userSelectdReducer,
    favorites: favoritesReducer
};
