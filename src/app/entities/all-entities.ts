export interface User {
    firstName: string;
    lastName: string;
    email: string;
    celphone: number;
    birthdate: Date;
    photo: string;
    gender: string;
    age: number;
    description: string;
    favorite: boolean;
    id: string;
    userType: string;
    password: string;
}
