import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

import { User } from '@entities/all-entities';

@Component({
  selector: 'app-item-user',
  templateUrl: './item-user.component.html',
  styleUrls: ['./item-user.component.scss']
})
export class ItemUserComponent {

  @Input() user: User;
  @Input() userSelected: User;
  @Output() userE: EventEmitter<User> = new EventEmitter<User>();
  @Output() deleteUser: EventEmitter<User> = new EventEmitter<User>();
  @Output() changeFavorite: EventEmitter<User> = new EventEmitter<User>();

  constructor(private router: Router) { }

  get isSelected(): boolean {
    return this.user?.id === this.userSelected?.id;
  }

  get isStar(): boolean {
    return this.router.url.includes('star')
  }

  public goToCrud(param: string, user: User): void {
    this.router.navigate([`crud/${param}/${user.id}`]);
  }

  public addFavorite(user: User): void {
    const data: User = {
      ...user,
      favorite: !user.favorite
    };
    this.changeFavorite.emit(data);
  }
}
