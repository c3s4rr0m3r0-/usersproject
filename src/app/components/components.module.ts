import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ItemUserComponent } from '@components/item-user/item-user.component';

@NgModule({
  declarations: [
    ItemUserComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    ItemUserComponent
  ]
})
export class ComponentsModule { }
