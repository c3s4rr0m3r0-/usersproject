import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

import { User } from 'src/app/entities/all-entities';
import { AppFacade } from '@app/app.facade';
import { Observable, Subscription } from 'rxjs';

import * as CryptoJS from 'crypto-js';
import { CoreService } from '@app/services/core.service';


@Component({
  selector: 'app-crud',
  templateUrl: './crud.component.html',
  styleUrls: ['./crud.component.scss']
})
export class CrudComponent implements OnInit, OnDestroy {

  public formT: FormGroup;

  public id: number;
  public type: string;
  public subscription: Subscription[] = [];

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private facade: AppFacade,
    private route: ActivatedRoute,
    private core: CoreService) {
  }

  ngOnDestroy(): void {
    this.subscription.forEach(subs => subs.unsubscribe());
  }

  ngOnInit(): void {
    this.type = this.route.snapshot.paramMap.get('type');
    const id = this.route.snapshot.paramMap.get('id');
    this.initForm();
    this.id = parseInt(id, 0);
    if (this.type === 'update' && this.id !== null) {
      this.getUser(this.id);
      const observer = (user) => { this.initForm(user); };
      this.subscription.push(this.userSelected.subscribe(observer));
    }
  }

  get isCreate(): boolean {
    return this.type === 'create' ? true : false;
  }

  get userSelected(): Observable<User> {
    return this.facade.userSelected$;
  }

  get age(): number {
    const today = new Date();
    const birthdate = new Date(this.formT.get('birthdate').value);
    let age = today.getFullYear() - birthdate.getFullYear();
    const m = today.getMonth() - birthdate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthdate.getDate())) {
      age--;
    }
    return age;
  }

  get isBirthdayValid(): boolean {
    return this.age < 18 && this.formT.get('birthdate').touched ? true : false;
  }

  public initForm(data?: User): void {
    this.formT = this.fb.group({
      firstName: ['' || data?.firstName, [Validators.required, Validators.minLength(5)]],
      lastName: ['' || data?.lastName, [Validators.required, Validators.minLength(5)]],
      email: ['' || data?.email, [Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,3}$')]],
      celphone: ['' || data?.celphone, [Validators.required, Validators.pattern('^[0-9]*$')]],
      birthdate: ['' || data?.birthdate, Validators.required],
      gender: ['' || data?.gender, Validators.required],
      age: ['' || data?.age, [Validators.required, Validators.pattern('^[0-9]*$')]],
      description: ['' || data?.description, Validators.required],
      favorite: ['' || data?.favorite],
      photo: ['' || data?.photo],
      id: ['' || data?.id],
      userType: ['' || data?.userType, Validators.required],
      password: ['' || data?.password, Validators.required],
    });
    if (!this.isCreate) { this.formT.get('password').disable(); }
  }

  public getUser(id: number): void { this.facade.gethUserById(id); }

  public create(): void {
    if (this.validators()) {
      const data: User = {
        ...this.formT.value,
        favorite: false,
        id: Math.floor(Math.random() * 1000),
        password: this.core.encrypt(this.formT.get('password').value)
      };
      this.facade.createUser(data);
    }
  }

  public update(): void {
    if (this.validators()) {
      const data: User = {
        ...this.formT.value,
        password: this.core.encrypt(this.formT.get('password').value)
      };
      console.log(data);
      this.facade.updateUser(data);
    }
  }

  public isValid(param: string): boolean {
    return this.formT.get(param).invalid && this.formT.get(param).touched;
  }

  goBack(): void {
    this.router.navigate(['dashboard']);
  }

  private validators(): boolean {
    Object.values(this.formT.controls).forEach(item => {
      if (item instanceof FormControl) { item.markAsTouched(); }
    });
    return this.formT.invalid && this.age >= 18 ? false : true;
  }

}
