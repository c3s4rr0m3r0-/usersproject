import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { ComponentsModule } from '@components/components.module';
import { ReactiveFormsModule } from '@angular/forms';

import { CrudComponent } from '@modules/crud/crud.component';

const routes: Routes = [
  { path: '', component: CrudComponent }
];

@NgModule({
  declarations: [CrudComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    ReactiveFormsModule,
    ComponentsModule
  ]
})
export class CrudModule { }
