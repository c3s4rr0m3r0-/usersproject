import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

import { AppFacade } from '@app/app.facade';
import { User } from 'src/app/entities/all-entities';

interface ItemSilider {
  text: string;
  icon: string;
}

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  public itemsSlider: ItemSilider[] = [
    { text: 'Home', icon: 'fas fa-home' },
    { text: 'Start', icon: 'fas fa-star' }
  ];
  public users: User[] = [];

  constructor(
    private router: Router,
    private facade: AppFacade) { }

  ngOnInit(): void {
    this.facade.fetchUsers();
  }

  get userSelected$(): Observable<User> {
    return this.facade.userSelected$;
  }

  public isItemSelect(item: string): boolean {
    switch (item) {
      case ('Home'): {
        if (!this.router.url.includes('star')) {
          return true;
        }
        break;
      }
      case 'Start': {
        if (this.router.url.includes('star')) {
          return true;
        }
        break;
      }
    }
  }

  public goItem(item: string): void {
    if (item === 'Home') {
      this.router.navigate(['']);
    }
    if (item === 'Start') {
      this.router.navigate(['dashboard/star']);
    }
  }

}
