import { Component, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';

import { AppFacade } from '@app/app.facade';
import { User } from '@app/entities/all-entities';

@Component({
  selector: 'app-starts',
  templateUrl: './stars.component.html',
  styleUrls: ['./stars.component.scss']
})
export class StarsComponent implements OnInit {

  public subscription: Subscription[] = [];

  constructor(
    private facade: AppFacade
  ) { }

  ngOnInit(): void {
    const observer = (users: User[]) => {
      const favorites = users.filter(user => user.favorite);
      this.facade.sendFavorites(favorites);
    };
    this.subscription.push(
      this.users$.subscribe(observer)
    );
  }

  get userSelected$(): Observable<User> { return this.facade.userSelected$; }

  get users$(): Observable<User[]> { return this.facade.users$; }

  get favorites$(): Observable<User[]> { return this.facade.favorites$; }

  public userClick(user: User): void {
    this.facade.gethUserById(parseInt(user.id, 0));
  }

}
