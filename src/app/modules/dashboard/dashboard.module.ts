import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { ComponentsModule } from '@components/components.module';

import { DashboardComponent } from '@modules/dashboard/dashboard.component';
import { StarsComponent } from '@app/modules/dashboard/stars/stars.component';
import { HomeComponent } from '@modules/dashboard/home/home.component';
import { CoreService } from '@services/core.service';
import { AppFacade } from 'src/app/app.facade';

const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    children: [
      { path: '', component: HomeComponent },
      { path: 'star', component: StarsComponent },
    ]
  },
];

@NgModule({
  declarations: [
    DashboardComponent,
    StarsComponent,
    HomeComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    ComponentsModule
  ],
  providers: [CoreService, AppFacade],
})
export class DashboardModule { }
