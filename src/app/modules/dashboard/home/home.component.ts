import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

import { User } from '@entities/all-entities';
import { AppFacade } from 'src/app/app.facade';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent {

  constructor(
    private router: Router,
    private facade: AppFacade
  ) { }

  get users$(): Observable<User[]> { return this.facade.users$; }
  get userSelected$(): Observable<User> { return this.facade.userSelected$; }

  public userClick(user: User): void {
    this.facade.gethUserById(parseInt(user.id, 0));
  }

  public deleteUser(user: User): void {
    this.facade.deleteUser(parseInt(user.id, 0));
  }

  public goToCrud(param: string): void {
    this.router.navigate([`crud/${param}`]);
  }

  public changeFavorite(user: User): void {
    this.facade.changeFavorite(user);
  }

}
