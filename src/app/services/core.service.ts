import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { environment as ENV } from '@environment/environment';
import { User } from '@entities/all-entities';
import { HttpClient } from '@angular/common/http';

import * as CryptoJS from 'crypto-js';
import { tap } from 'rxjs/operators';

@Injectable()
export class CoreService {

  private secretKey = '123456$#@$^@1ERF';

  constructor(private http: HttpClient) { }

  public createUser(user: User): Observable<any> {
    return this.http.post(`${ENV.api}/data`, user);
  }

  public getUsers(): Observable<User[]> {
    return this.http.get<User[]>(`${ENV.api}/data`);
  }

  public getUserById(id: number): Observable<User> {
    return this.http.get<User>(`${ENV.api}/data/${id}`);
  }

  public updateUser(user: User): Observable<any> {
    return this.http.put(`${ENV.api}/data/${user.id}`, user);
  }

  public deleteUser(id: number): Observable<any> {
    return this.http.delete<User>(`${ENV.api}/data/${id}`).pipe(
      tap(x => console.log(x))
    );
  }

  public encrypt(value): string {
    const key = CryptoJS.enc.Utf8.parse(this.secretKey);
    const iv = CryptoJS.enc.Utf8.parse(this.secretKey);
    const encrypted = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(value.toString()), key,
      {
        keySize: 128 / 8,
        iv,
        mode: CryptoJS.mode.CBC,
        padding: CryptoJS.pad.Pkcs7
      });

    return encrypted.toString();
  }
}
