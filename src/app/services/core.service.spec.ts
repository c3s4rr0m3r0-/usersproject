import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { of } from 'rxjs';

import { User } from '@entities/all-entities';

import { CoreService } from './core.service';
import { environment as ENV } from '@environment/environment';

fdescribe('CoreService', () => {

  let httpMock: HttpTestingController;
  let service: CoreService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule, HttpClientTestingModule],
      providers: [CoreService]
    });
    service = TestBed.inject(CoreService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return an observable of Users', () => {
    const user: User[] = [
      {
        firstName: 'first name',
        lastName: 'last name',
        email: 'example@gmail.com',
        celphone: 3002145,
        birthdate: new Date(),
        photo: '',
        gender: 'male',
        age: 10,
        description: 'This is a description',
        favorite: false,
        id: '1',
        userType: 'Administrator',
        password: '123',
      }
    ];
    service.getUsers().subscribe(users => {
      expect(users).toEqual(user);
    });
    const rqt = httpMock.expectOne(`${ENV.api}/data`);
    expect(rqt.request.method).toBe('GET');
    rqt.flush(user);
  });

  it('should CREATE an users', () => {
    const user: User = {
      firstName: 'first name',
      lastName: 'last name',
      email: 'example@gmail.com',
      celphone: 3002145,
      birthdate: new Date(),
      photo: '',
      gender: 'male',
      age: 10,
      description: 'This is a description',
      favorite: false,
      id: '1',
      userType: 'Administrator',
      password: '123',
    };
    service.createUser(user).subscribe(resp => {
      expect(resp).toEqual(user);
    });
    const rqt = httpMock.expectOne(`${ENV.api}/data`);
    expect(rqt.request.method).toBe('POST');
    rqt.flush(user);
  });

  it('should UPDATE an user', () => {
    const user: User = {
      firstName: 'first name',
      lastName: 'last name',
      email: 'example@gmail.com',
      celphone: 3002145,
      birthdate: new Date(),
      photo: '',
      gender: 'male',
      age: 10,
      description: 'This is a description',
      favorite: false,
      id: '1',
      userType: 'Administrator',
      password: '123',
    };
    service.updateUser(user).subscribe(resp => {
      expect(resp).toEqual(user);
    });
    const rqt = httpMock.expectOne(`${ENV.api}/data/${user.id}`);
    expect(rqt.request.method).toBe('PUT');
    rqt.flush(user);
  });

  it('should DELETE an user', () => {
    service.deleteUser(1).subscribe(resp => {
      expect(resp).toEqual({});
    });
    const rqt = httpMock.expectOne(`${ENV.api}/data/1`);
    expect(rqt.request.method).toBe('DELETE');
    rqt.flush({});
  });
});
