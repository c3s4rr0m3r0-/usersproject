import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

import { StateApp } from '@store/state/state-app';
import { UsersEffects } from '@store/effects/effects-app';
import { AppFacade } from './app.facade';

import { CoreService } from '@services/core.service';

import { environment as ENV } from '@environment/environment';

import { AppComponent } from './app.component';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    StoreModule.forRoot(StateApp),
    EffectsModule.forRoot([UsersEffects]),
    StoreDevtoolsModule.instrument({
      maxAge: 25,
      logOnly: ENV.production,
    }),
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [CoreService, AppFacade],
  bootstrap: [AppComponent]
})
export class AppModule { }
